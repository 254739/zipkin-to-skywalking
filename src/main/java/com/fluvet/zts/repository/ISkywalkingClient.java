package com.fluvet.zts.repository;

import com.fluvet.zts.model.SkywalkingSpan;
import lombok.ToString;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import reactor.core.publisher.Mono;

@HttpExchange(url = "/v3/segment", accept = "application/json", contentType = "application/json")
public interface ISkywalkingClient {

    @PostExchange()
    Mono<ResponseEntity<Void>> save(@RequestBody SkywalkingSpan span);

}