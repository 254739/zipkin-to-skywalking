package com.fluvet.zts.service;

import com.fluvet.zts.model.ZipkinSpan;

public interface IApiService {

    /**
     * 转换
     *
     */
    boolean transform(ZipkinSpan span);

}
