package com.fluvet.zts.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fluvet.zts.repository.ISkywalkingClient;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class WebConfig {

    @Value("${skywalking-url}")
    String skywalkingUrl;

    @Bean
    WebClient webClient(ObjectMapper objectMapper) {
        return WebClient.builder()
                .baseUrl(skywalkingUrl)
                .build();
    }

    @SneakyThrows
    @Bean
    ISkywalkingClient postClient(WebClient webClient) {
        HttpServiceProxyFactory httpServiceProxyFactory =
                HttpServiceProxyFactory.builder(WebClientAdapter.forClient(webClient))
                        .build();
        return httpServiceProxyFactory.createClient(ISkywalkingClient.class);
    }

}