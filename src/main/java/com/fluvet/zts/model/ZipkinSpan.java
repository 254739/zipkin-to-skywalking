package com.fluvet.zts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@NoArgsConstructor
@Data
public class ZipkinSpan {

    @JsonProperty("id")
    private String id;

    @JsonProperty("traceId")
    private String traceId;

    @JsonProperty("parentId")
    private String parentId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("timestamp")
    private Long timestamp;

    @JsonProperty("duration")
    private Integer duration;

    @JsonProperty("kind")
    private String kind;

    @JsonProperty("localEndpoint")
    private LocalEndpointDTO localEndpoint;

    @JsonProperty("remoteEndpoint")
    private RemoteEndpointDTO remoteEndpoint;

    @JsonProperty("tags")
    private Map<String, String> tags;

    @NoArgsConstructor
    @Data
    public static class LocalEndpointDTO {

        @JsonProperty("serviceName")
        private String serviceName;

        @JsonProperty("ipv4")
        private String ipv4;

        @JsonProperty("port")
        private Integer port;

    }

    @NoArgsConstructor
    @Data
    public static class RemoteEndpointDTO {

        @JsonProperty("ipv4")
        private String ipv4;

        @JsonProperty("port")
        private Integer port;

    }

}
