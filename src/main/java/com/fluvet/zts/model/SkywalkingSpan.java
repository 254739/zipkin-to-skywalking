package com.fluvet.zts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SkywalkingSpan {

    @JsonProperty("traceId")
    private String traceId;

    @JsonProperty("serviceInstance")
    private String serviceInstance;

    @JsonProperty("spans")
    private List<SpansDTO> spans;

    @JsonProperty("service")
    private String service;

    @JsonProperty("traceSegmentId")
    private String traceSegmentId;

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class SpansDTO {

        @JsonProperty("operationName")
        private String operationName;

        @JsonProperty("startTime")
        private Long startTime;

        @JsonProperty("endTime")
        private Long endTime;

        @JsonProperty("spanType")
        private String spanType;

        @JsonProperty("spanId")
        private Integer spanId;

        @JsonProperty("isError")
        private Boolean isError;

        @JsonProperty("parentSpanId")
        private Integer parentSpanId;

        @JsonProperty("componentId")
        private Integer componentId;

        @JsonProperty("peer")
        private String peer;

        @JsonProperty("spanLayer")
        private String spanLayer;

        @JsonProperty("tags")
        private List<TagsDTO> tags;

        @NoArgsConstructor
        @Data
        public static class TagsDTO {

            @JsonProperty("key")
            private String key;

            @JsonProperty("value")
            private String value;

        }

    }

}
