package com.fluvet.zts.controller;

import com.fluvet.zts.model.ZipkinSpan;
import com.fluvet.zts.service.IApiService;
import jakarta.annotation.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Resource
    IApiService iApiService;

    @PostMapping("/v2/spans")
    public ResponseEntity<String> saveCar(@RequestBody List<ZipkinSpan> zipkinSpanList) {
        for (ZipkinSpan zipkinSpan : zipkinSpanList) {
            boolean isSuccess = iApiService.transform(zipkinSpan);
            if (!isSuccess) {
                return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>("", HttpStatus.ACCEPTED);
    }

}
