# 下面镜像和openjdk:17.0.2-oraclelinux8镜像是一样的，弄到阿里云加快速度
# FROM registry.cn-shanghai.aliyuncs.com/fluvet/openjdk:17
FROM openjdk:17.0.2-oraclelinux8
COPY ./target/*.jar /app/app.jar
# 在启动jar包添加时区参数：-Duser.timezone=GMT+08防止时间错误问题
ENTRYPOINT java -Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom -jar -Duser.timezone=GMT+08 /app/app.jar